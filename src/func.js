const getSum = (str1, str2) => {
    if (typeof str1 !== 'string' || typeof str2 !== 'string') return false;

    var ar1 = str1.length == 0 ? [0] : Array.from(str1);
    var ar2 = str2.length == 0 ? [0] : Array.from(str2);
    var result = []

    if (ar1.some(c => isNaN(c)) || ar2.some(c => isNaN(c))) return false;

    for (var i = 0; i < Math.max(ar1.length, ar2.length); i++) {
        result.push((parseInt(ar1[i]) + parseInt(ar2[i])).toString())
        if (ar1.length != ar2.length) {
            if (i + 1 >= ar1.length) {
                result = result.concat(ar2.slice(i + 1)); break
            }
            if (i + 1 >= ar2.length) {
                result = result.concat(ar1.slice(i + 1)); break
            }
        }
    }
    return result.join('');
};

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
    var postsByAuthor = listOfPosts.filter(p => p.author === authorName).length.toString();

    var commentsByAuthor = listOfPosts.filter(p => p.comments != null).map(c => c.comments.filter(cc => cc.author === authorName).length).reduce((a, b) => a + b).toString();

    return 'Post:' + postsByAuthor + ',comments:' + commentsByAuthor;
};

const tickets = (people) => {
    var bills = [0, 0, 0];

    for (var bill of people) {
        switch (bill.toString()) {
            case '25': { bills[0]++; break; }
            case '50': {
                if (bills[0] < 1) return 'NO'
                else { bills[0]--; bills[1]++; break; }
            }
            case '100': {
                if (bills[0] >= 1 && bills[1] >= 1 || bills[0] >= 3) {
                    bills[2]++;
                    if (bills[1] > 0) { bills[1]--; bills[0]--; }
                    else (bills[0] = bills[0] - 3)
                }
                else {
                    return 'NO';
                }
            }
        }
    }
    return 'YES';
}

module.exports = { getSum, getQuantityPostsByAuthor, tickets };
